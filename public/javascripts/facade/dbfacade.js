var mongoose = require ("mongoose"); // The reason for this demo.
var dbFacade;
// Here we find an appropriate database to connect to, defaulting to
// localhost if we don't find one.
var dbfacade = function() {
    // run code here, or...
    var uristring =
        process.env.MONGOLAB_URI ||
        process.env.MONGOHQ_URL ||
        'mongodb://localhost/feeddb';

// The http server will listen to an appropriate port, or default to
// port 5000.
    var theport = process.env.PORT || 5000;

// Makes connection asynchronously.  Mongoose will queue up database
// operations and release them when the connection is complete.
    mongoose.connect(uristring, function (err, res) {
        if (err) {
            console.log ('ERROR connecting to: ' + uristring + '. ' + err);
        } else {
            console.log ('Succeeded connected to: ' + uristring);
        }
    });

    var userSchema = new mongoose.Schema({
        aulyUpload: {
            timingArr: [],
            lyrics: { type: String, trim: true }
        },
    });
    dbFacade = mongoose.model('PowerUsers', userSchema);
};



dbfacade.prototype.addToDb= function(json) {
    // Creating one auLy.
    console.log("db","json "+json);
    var dbObject = new dbFacade ({
        name: { first: 'John', last: 'Doe' },
        age: 25
    });

    // Saving it to the database.
    dbObject.save(function (err) {if (err) console.log ('Error on save!')});

};

// Clear out old data
dbFacade.remove({}, function(err) {
    if (err) {
        console.log ('error deleting old data.');
    }
});



